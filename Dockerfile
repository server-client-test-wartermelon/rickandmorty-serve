FROM node:carbon

ENV SECRETO = supersecreto

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . ./

EXPOSE 4000 6379


CMD ["./wait-for-it.sh", "npm", "start"] 