import { ApolloServer } from 'apollo-server-express';
import { resolvers } from './data/resolvers';
import { typeDefs } from './data/schema';
import jwt from 'jsonwebtoken';
import express from 'express';
import config from './config/config';
//generar token
import * as dotenv from 'dotenv';
dotenv.config();

const app = express();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {

    // Obtener el token enviado desde el cliente
    const token = req.headers['autorization'];
    if (token !== 'null') {
      try {
        // verificamos el token del front-end [cliente]
        const usuarioActual = await jwt.verify(token, process.env.SECRETO);

        // agregamos el usuario actual al request
        req.usuarioActual = usuarioActual;

        return {
          usuarioActual
        }
      } catch (error) {
        console.error(error)
      }
    }
  }
});

// Aca es donde se conecta apollo server con express
server.applyMiddleware({ app });

app.listen({ port: config.PORT }, () => {
  console.log(`Servidor corriendo en puerto http://localhost:${config.PORT}${server.graphqlPath}`);
});
