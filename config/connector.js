const _config = require('./config');


var Connector = {
  _redis: null,

  Redis: function () {
    if (Connector._redis == null) {
      var redis = require('redis');
      Connector._redis = redis.createClient({ host: _config.redis.host, port: _config.redis.port});
      Connector._redis.select(_config.redis.db, function (err, resp) {
        console.log(`Connect DB ${resp}!`);
      });

      if (_config.redis.password != "") {
        Connector._redis.auth(_config.redis.password, function () {
          console.log('Authenticated...');
        });
      }

      Connector._redis.on('error', function (err) {
        console.log('Error Connecting: ' + err);
      });
    }
    return Connector._redis;
  }
};


module.exports = Connector;