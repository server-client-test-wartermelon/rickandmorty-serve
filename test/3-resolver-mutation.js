'use strict';

const EasyGraphQLTester = require('easygraphql-tester');
const fs = require('fs');
const path = require('path');

const apiSchema = fs.readFileSync(path.join(__dirname, './../data/schema', 'schema.graphql'), 'utf8');
const resolvers = require('./mocks/resolvers/mutation').resolversMutation;
const expect = require('chai').expect;

describe("Testing Resolver - Type Root Mutation", () => {

  let tester;
  before(function () {
    tester = new EasyGraphQLTester(apiSchema, resolvers);
  });
  it("Comprobar que 'crearUsuario' devuelve correctamente", async () => {
    const mutation = `
    mutation crearUsuario($nombre: String!, $usuario: String!, $password: String! ){
      crearUsuario(nombre: $nombre, usuario: $usuario, password: $password)
    }`;

    const result = await tester.graphql(mutation, undefined, undefined, { nombre: 'Marcos', usuario: 'marcos01', password: '******' });

    expect(result.data.crearUsuario).to.be.a('string');
    expect(result.data.crearUsuario).to.eql('Creado Correctamente');

  });
  it("Comprobar que 'autenticarUsuario' devuelve el token correctamente", async () => {
    const mutation = `
    mutation autenticarUsuario($usuario: String!, $password: String!){
      autenticarUsuario(usuario: $usuario, password: $password){
        token
      }
    }`;

    const result = await tester.graphql(mutation, undefined, undefined, {usuario: 'marcos01', password: '******'});

    expect(result.data.autenticarUsuario).to.be.a('object');
    
  });
});