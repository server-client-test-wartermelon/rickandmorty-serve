'use strict';

const EasyGraphQLTester = require('easygraphql-tester');
const fs = require('fs');
const path = require('path');

const apiSchema = fs.readFileSync(path.join(__dirname, './../data/schema', 'schema.graphql'), 'utf8');

// const tester = new EasyGraphQLTester(apiSchema);
describe("Test Schema GraphQl", () => {
  let tester;

  before(function () {
    tester = new EasyGraphQLTester(apiSchema);
  });
  describe("Type Root: Query", () => {

    it("'obtenerUsuario' Valido", () => {
      const query = `{obtenerUsuario{id, usuario, nombre}}`;
      tester.test(true, query, {});
    });

    it("'obtenerUsuario' Invalido", () => {
      const query = `{obtenerUsuario}`;
      tester.test(false, query, {});
    });

    it("'obtenerCharacters' Valido", () => {
      const query = `
      query	obtenerCharacters($value: String){
        obtenerCharacters(value: $value){
          id
          name
          species
          status
          gender
          image
        }
      }`;
      tester.test(true, query, { value: "1" });
    });

    it("'obtenerCharacters' Invalido", () => {
      const query = `
      query	obtenerCharacters($value: String){
        obtenerCharacters(value: $value){
          id
          name
          species
          status
          gender
          image
        }
      }`;
      tester.test(false, query, { value: 1 });
    });
  });
  describe("Type Root: Mutation", () => {

    it("'crearUsuario' Valido", () => {
      const mutation = `
      mutation crearUsuario($nombre: String!, $usuario: String!, $password: String! ){
        crearUsuario(nombre: $nombre, usuario: $usuario, password: $password)
      }`;
      tester.test(true, mutation, { nombre: "Marcos Llumiquinga", usuario: "marcos01", password: "******" });
    });

    it("'crearUsuario' Invalido", () => {
      const mutation = `
      query crearUsuario($nombre: String!, $usuario: String!, $password: String! ){
        crearUsuario(nombre: $nombre, usuario: $usuario, password: $password)
      }`;
      tester.test(false, mutation, { nombre: "Marcos Llumiquinga", usuario: "marcos01" });
    });
  });
});