const obtenerUsuario = () => {
  return {
    id: '123456',
    usuario: 'marcos01',
    nombre: 'Marcos Llumiquinga'
  }
}

const obtenerCharacters = () => {
  return [
    {
      "id": 481,
      "name": "Retired General Rick",
      "species": "Human",
      "status": "unknown",
      "gender": "Male",
      "image": "https://rickandmortyapi.com/api/character/avatar/481.jpeg"
    }
  ]
}

const resolverQueries = {
  Query: {
    obtenerUsuario,
    obtenerCharacters
  }
}

module.exports = {
  resolverQueries
}