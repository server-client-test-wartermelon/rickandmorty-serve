'use strict';

const EasyGraphQLTester = require('easygraphql-tester');
const fs = require('fs');
const path = require('path');
const resolver = require('./mocks/resolvers/query').resolverQueries;
const expect = require('chai').expect;

const apiSchema = fs.readFileSync(path.join(__dirname, './../data/schema', 'schema.graphql'), 'utf8');

describe("Testing Resolver - Type Root Query", () => {

  let tester;
  before(function () {
    tester = new EasyGraphQLTester(apiSchema, resolver);
  });
  it("Comprobar que 'obtenerUsuario' devuelve correctamente y es un Object", async () => {
    const query = `{obtenerUsuario{id, usuario, nombre}}`;
    const result = await tester.graphql(query, undefined, undefined, {});

    expect(result.data.obtenerUsuario).to.be.a('object');
    expect(result.data.obtenerUsuario).to.eql({
      id: '123456',
      usuario: 'marcos01',
      nombre: 'Marcos Llumiquinga'
    });
  });
  it("Comprobar que 'obtenerCharacters' devuelve correctamente y es un Array de Object", async () => {
    const query = `query	obtenerCharacters($value: String){
      obtenerCharacters(value: $value){
        id
        name
        species
        status
        gender
        image
      }
    }`;
    const result = await tester.graphql(query, undefined, undefined, {});

    expect(result.data.obtenerCharacters).to.be.a('array');
    expect(result.data.obtenerCharacters).to.eql([
      {
        "id": 481,
        "name": "Retired General Rick",
        "species": "Human",
        "status": "unknown",
        "gender": "Male",
        "image": "https://rickandmortyapi.com/api/character/avatar/481.jpeg"
      }
    ]);
    expect(result.data.obtenerCharacters).not.to.eql([
      {
        "id": "481",
        "name": "Retired General Rick",
        "species": "Human",
      }
    ]);
  });
});