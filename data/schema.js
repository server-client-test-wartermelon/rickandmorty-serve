import { importSchema } from 'graphql-import';

const typeDefs = importSchema('data/schema/schema.graphql');

export { typeDefs };