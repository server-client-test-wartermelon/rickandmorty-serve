import Connector from '../config/connector';
import bluebird from 'bluebird';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import axios from 'axios';

//generar token
import * as dotenv from 'dotenv';
dotenv.config();

// Promete todo el objeto pasando por las propiedades del objeto y creando un equivalente asíncrono de cada función en el objeto y su cadena de prototipo.
bluebird.promisifyAll(Connector.Redis());

// const client = redis.createClient(); //creates a new client
const client = Connector.Redis();

// Crea el token con la informacion del USUARIO.
const crearToken = (usuarioLogin, secreto, expiresIn) => {
  return jwt.sign({ usuario: usuarioLogin }, secreto, { expiresIn });
}

export const resolvers = {
  Query: {
    //  En este caso le pasamos el token por headers, (index.js) lo recibimos por context, verificamos el token y si es valido, agregamos la informacion del usuario verificada al req de la peticion para proceder a hacer la busqueda segun usuario!
    // En el Cliente, si el usuario no esta autenticado, (no existe ese usuario), inmediatamente es redireccionado al login.
    // Este endpoint se ejecuta en cada vista de la App (Cliente)
    obtenerUsuario: async (root, args, { usuarioActual }) => {

      if (!usuarioActual) {
        return null;
      }

      var usuario;

      await client.hgetallAsync(usuarioActual.usuario).then(res => {
        usuario = res;
      }).catch(console.log);

      return usuario;
    },
    obtenerCharacters: async (root, { value }) => {

      let page = 0;
      if (Number(value) > 0) {
        page = page + Number(value);
      } else {
        page = 1;
      }
      return await axios.get(`https://rickandmortyapi.com/api/character/?page=${page}`).then(items => {

        // Mapeo los resultados y solo retorno las propiedades Solicitadas
        // (nombres, estado, especies, género e imagen estan en obj)
        return items.data.results.map(({ created, url, episode, origin, type, location, ...obj }) => obj);
      });
    }
  },
  Mutation: {
    crearUsuario: async (root, { nombre, usuario, password }) => {
      var error;

      // Consulto si ese USUARIO ya existe en la DB.
      await client.existsAsync(usuario).then(res => {
        if (res === 1) {
          error = 1;
          return;
        }
      }).catch(console.log);

      // Si no existe, lo creo.
      await client.hmsetAsync(usuario, {
        'id': require('crypto').randomBytes(10).toString('hex'),
        'nombre': nombre,
        'usuario': usuario,
        'password': bcrypt.hashSync(password, 10),
      }).catch(console.log);

      if (error === 1) throw new Error('El Usuario ya Existe');
      if (error === 0) throw new Error('Usuario no creado, intentelo nuevamente');
      return "Creado Correctamente";
    },
    autenticarUsuario: async (root, { usuario, password }) => {

      var error;

      // Valido que el usuario EXISTA en la db
      await client.hgetallAsync(usuario).then(async res => {
        if (!res) {
          error = 1;
          return;
        }

        // Comparo su password
        const passwordCorrecto = await bcrypt.compareSync(password, res.password);

        if (!passwordCorrecto) {
          error = 2;
          return;
        }
      }).catch(console.log);;


      if (error === 1) throw new Error('Usuario No Encontrado');
      if (error === 2) throw new Error('Password Incorrecto');

      // Retorno el TOKEN del usuario con su info.
      return {
        token: crearToken(usuario, process.env.SECRETO, '2m')
      }
    }
  }
}

export default resolvers;